<?php

return [
    'name' => 'Tiers',
    'model' => 'anima\firefly\Http\models\Tiers',
    'icon' => 'fa-layer-group ',
    'index' => '/admin/tiers',
    'gate' => 'viewAny',
    'section' => [
        'id' => 4,
        'name' => 'Users',
    ],
];
