<?php

return [
    'name' => 'Users',
    'model' => 'anima\firefly\Http\models\User',
    'icon' => 'fa-user ',
    'index' => '/admin/users',
    // "gate" => 'viewAny'
    'section' => [
        'id' => 4,
        'name' => 'Users',
    ],
];
