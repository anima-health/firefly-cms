<?php

return [
    'name' => 'Menus',
    'model' => 'anima\firefly\Http\models\Menu',
    'icon' => 'fa-address-card ',
    'index' => '/admin/menus',
    'gate' => 'viewAny',
    'section' => [
        'id' => 3,
        'name' => 'Site',
    ],
];
