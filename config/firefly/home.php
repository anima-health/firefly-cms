<?php

return [
    'name' => 'Home',
    'icon' => 'fa-house ',
    'index' => '/admin/',
    'section' => [
        'id' => 1,
        'name' => 'Dashboard',
    ],
];
