<?php

return [
    'name' => 'Settings',
    // 'model' => 'anima\firefly\Http\models\Role',
    'icon' => 'fa-gear ',
    'index' => '/admin/settings',
    // 'gate' => 'viewAny',
    'section' => [
        'id' => 3,
        'name' => 'Site',
    ],
];
