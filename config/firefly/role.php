<?php

return [
    'name' => 'Roles',
    'model' => 'anima\firefly\Http\models\Role',
    'icon' => 'fa-address-card ',
    'index' => '/admin/roles',
    'gate' => 'viewAny',
    'section' => [
        'id' => 4,
        'name' => 'Users',
    ],
];
