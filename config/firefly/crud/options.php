<?php

return [
    'Page' => [
        'template' => [
            'options' => [
                'default',
                'no footer',
            ],
            'default' => 'default',
        ],
    ],
    'User' => [
        'account_type' => [
            'options' => [
                'Personal',
                'Business',
            ],
            'default' => 'Personal',
        ],
        'gender' => [
            'options' => [
                'Male',
                'Female',
                'Other',
            ],
            'default' => 'Male',
        ],
        'marketing' => [
            'options' => [
                'All',
                'Marketing',
                'None',
            ],
            'default' => 'None',
        ],
        'roles' => [
            'options' => [
                'None',
                'Landlord Super User',
                'Landlord Admin',
                'Landlord Tester',
                'Tenant Super User',
                'Tenant Admin',
                'Tenant Tester',
            ],
            'default' => 'None',
        ],
    ],
];
