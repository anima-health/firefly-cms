<?php

return [
    'name' => 'Tenants',
    'model' => 'anima\firefly\Http\models\Tenant',
    'icon' => 'fa-hotel ',
    'index' => '/admin/tenants',
    'gate' => 'viewAny',
    'section' => [
        'id' => 4,
        'name' => 'Users',
    ],
];
