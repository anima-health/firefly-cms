<?php

return [
    'name' => 'Page',
    'model' => 'App\Models\Page',
    'icon' => 'fa-file ',
    'index' => '/admin/pages',
    'gate' => 'viewAny',
    'section' => [
        'id' => 2,
        'name' => 'Content',
    ],
];
