<?php

return [
    'name' => 'newsletter',
    'fields' => [
        'title' => [
            'type' => 'text',
            'label' => 'Title',
            'value' => '',
            'required' => true,
        ],
        'content' => [
            'type' => 'textarea',
            'label' => 'Content',
            'value' => '',
            'required' => true,
        ],
    ],
];
