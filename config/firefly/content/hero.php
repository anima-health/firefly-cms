<?php

return [
    'name' => 'hero',
    'fields' => [
        'title' => [
            'type' => 'text',
            'label' => 'Title',
            'value' => '',
            'required' => true,
        ],
        'content' => [
            'type' => 'textarea',
            'label' => 'Content',
            'value' => '',
        ],
        'buttons' => [
            'type' => 'repeater',
            'label' => 'Buttons',
            'fields' => [
                'text' => [
                    'type' => 'text',
                    'label' => 'Text',
                    'value' => '',
                    'required' => true,
                ],
                'hover_text' => [
                    'type' => 'text',
                    'label' => 'Hover Text',
                    'value' => '',
                ],
                'url' => [
                    'type' => 'text',
                    'label' => 'URL',
                    'value' => '',
                    'required' => true,
                ],
                'opens_in' => [
                    'type' => 'select',
                    'label' => 'Opens In',
                    'options' => [
                        'current tab' => 'Current Tab',
                        'new tab' => 'New Tab',
                    ],
                    'value' => 'current tab',
                ],
            ],
        ],
        'image' => [
            'type' => 'image',
            'label' => 'Image',
            'value' => '',
        ],
    ],
];
