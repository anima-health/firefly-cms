<?php

return [
    'name' => 'cards',
    'fields' => [
        'title' => [
            'type' => 'text',
            'label' => 'Title',
            'value' => '',
            'required' => true,
        ],
        'background_color' => [
            'type' => 'select',
            'label' => 'Background Color',
            'options' => [
                'green' => 'Green',
                'midnight blue' => 'Midnight Blue',
                'teal' => 'Teal',
            ],
            'value' => 'white',
        ],
        'cards' => [
            'type' => 'repeater',
            'label' => 'Cards',
            'fields' => [
                'title' => [
                    'type' => 'text',
                    'label' => 'Title',
                    'value' => '',
                    'required' => true,
                ],
                'icon' => [
                    'type' => 'select',
                    'label' => 'Icon',
                    'value' => '',
                    'options' => [
                        'checklist' => 'Checklist',
                        'conversation' => 'Conversation',
                        'goal' => 'Goal',
                        'journal' => 'Journal',
                        'heart' => 'Heart',
                        'dumbbell' => 'Dumbbell',
                        'notes' => 'Notes',
                        'piechart' => 'Pie Chart',
                    ],
                ],
                'banner' => [
                    'type' => 'select',
                    'label' => 'Banner',
                    'value' => '',
                    'options' => [
                        'none' => 'None',
                        'coming' => 'Coming Soon',
                        'assess' => 'Assess',
                        'discover' => 'Discover',
                        'explore' => 'Explore',
                        'grow' => 'Grow',
                        'learn' => 'Learn',
                        'move' => 'Move',
                        'track' => 'Track',
                        'quest' => 'Quest',
                    ],
                ],
                'content' => [
                    'type' => 'textarea',
                    'label' => 'Content',
                    'value' => '',
                ],
            ],
        ],
    ],
];
