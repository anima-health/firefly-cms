<?php

return [
    'name' => 'table',
    'fields' => [
        'title' => [
            'type' => 'text',
            'label' => 'Title',
            'value' => '',
            'required' => true,
        ],
        'bg_color' => [
            'type' => 'select',
            'label' => 'Background Color',
            'options' => [
                'green' => 'Green',
                'midnight blue' => 'Midnight Blue',
                'teal' => 'Teal',
            ],
            'value' => 'white',
        ],
        'table' => [
            'type' => 'repeater',
            'label' => 'Table Rows',
            'fields' => [
                'columns' => [
                    'type' => 'repeater',
                    'label' => 'Columns',
                    'fields' => [
                        'column' => [
                            'type' => 'text',
                            'label' => 'Column',
                            'value' => '',
                            'required' => true,
                        ],
                    ],
                ],
            ],
        ],
    ],
];
