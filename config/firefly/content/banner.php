<?php

return [
    'name' => 'banner',
    'fields' => [
        'title' => [
            'type' => 'text',
            'label' => 'Title',
            'value' => '',
            'required' => true,
        ],
        'background_color' => [
            'type' => 'select',
            'label' => 'Background Color',
            'options' => [
                'green' => 'Green',
                'midnight blue' => 'Midnight Blue',
                'teal' => 'Teal',
            ],
            'value' => 'teal',
        ],
        'image' => [
            'type' => 'image',
            'label' => 'Image',
            'value' => '',
        ],
    ],
];
