<?php

return [
    'name' => 'content',
    'fields' => [
        'title' => [
            'type' => 'text',
            'label' => 'Title',
            'value' => '',
            'required' => true,
        ],
        'background_color' => [
            'type' => 'select',
            'label' => 'Background Color',
            'options' => [
                'green' => 'Green',
                'midnight blue' => 'Midnight Blue',
                'teal' => 'Teal',
            ],
            'value' => 'teal',
        ],
        'watermark' => [
            'type' => 'boolean',
            'label' => 'Watermark',
            'value' => false,
        ],
        'text_align' => [
            'type' => 'select',
            'label' => 'Text Align',
            'options' => [
                'left' => 'Left',
                'center' => 'Center',
                'right' => 'Right',
            ],
            'value' => 'left',
        ],
        'content' => [
            'type' => 'textarea',
            'label' => 'Content',
            'value' => '',
            'required' => true,
        ],
        'quotes' => [
            'type' => 'repeater',
            'label' => 'Quotes',
            'fields' => [
                'quote' => [
                    'type' => 'text',
                    'label' => 'Quote',
                    'value' => '',
                    'required' => true,
                ],
                'source' => [
                    'type' => 'text',
                    'label' => 'Source',
                    'value' => '',
                ],
                'alignment' => [
                    'type' => 'select',
                    'label' => 'Alignment',
                    'options' => [
                        'left' => 'Left',
                        'center' => 'Center',
                        'right' => 'Right',
                    ],
                    'value' => 'left',
                ],
                'vertical_alignment' => [
                    'type' => 'select',
                    'label' => 'Vertical Alignment',
                    'options' => [
                        'top' => 'Top',
                        'bottom' => 'Bottom',
                    ],
                    'value' => 'top',
                ],
            ],
        ],
        'image' => [
            'type' => 'image',
            'label' => 'Image',
            'fields' => [
                'position' => [
                    'type' => 'select',
                    'label' => 'Image Position',
                    'options' => [
                        'left' => 'Left',
                        'right' => 'Right',
                    ],
                    'value' => 'left',
                ],
                'square' => [
                    'type' => 'boolean',
                    'label' => 'Square Image',
                ],
                'alt' => [
                    'type' => 'text',
                    'label' => 'Alt Text',
                    'value' => '',
                ],
            ],
            'value' => '',
        ],
        'buttons' => [
            'type' => 'repeater',
            'label' => 'Buttons',
            'fields' => [
                'text' => [
                    'type' => 'text',
                    'label' => 'Text',
                    'value' => '',
                    'required' => true,
                ],
                'hover_text' => [
                    'type' => 'text',
                    'label' => 'Hover Text',
                    'value' => '',
                ],
                'url' => [
                    'type' => 'text',
                    'label' => 'URL',
                    'value' => '',
                    'required' => true,
                ],
                'opens_in' => [
                    'type' => 'select',
                    'label' => 'Opens In',
                    'options' => [
                        'current tab' => 'Current Tab',
                        'new tab' => 'New Tab',
                    ],
                    'value' => 'current tab',
                ],
            ],
        ],
    ],
];
