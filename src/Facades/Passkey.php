<?php

namespace anima\firefly\Facades;

use Illuminate\Support\Facades\Facade;

class Passkey extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'passkey'; // Matches the key in the service container
    }
}
