<?php

namespace anima\firefly\Facades;

use Illuminate\Support\Facades\Facade;

class FireflyMenu extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'fireflymenu'; // Matches the key in the service container
    }
}
