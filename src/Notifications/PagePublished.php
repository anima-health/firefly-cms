<?php

namespace anima\firefly\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class PagePublished extends Notification
{
    use Queueable;

    protected $pageTitle;

    /**
     * Create a new notification instance.
     */
    public function __construct($pageTitle)
    {
        $this->pageTitle = $pageTitle;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            'isAdmin' => true,
            'page_title' => $this->pageTitle,
            'message' => "The page '{$this->pageTitle}' has been published.",
        ];
    }
}
