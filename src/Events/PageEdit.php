<?php

namespace anima\firefly\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class PageEdit implements ShouldBroadcastNow
{
    use InteractsWithSockets, SerializesModels;

    public $message;

    // Constructor accepts the message we want to broadcast
    public function __construct($message)
    {
        $this->message = $message;
    }

    // Defines the Redis channel to broadcast on
    public function broadcastOn()
    {
        return new Channel('notifications');  // The channel name
    }

    // Defines the event's data
    public function broadcastWith()
    {
        return [
            'user' => Auth::user()->select('id', 'fname', 'lname')->first()->toArray(),
            'message' => $this->message,
        ];
    }
}
