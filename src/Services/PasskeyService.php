<?php

namespace anima\firefly\Services;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Ramsey\Uuid\UuidFactory;

class PasskeyService
{
    /**
     * Generate a new unique passkey.
     *
     * This method uses the UuidFactory to create a version 4 UUID, which is then
     * converted to a string and returned as the passkey.
     *
     * @return string The generated passkey.
     */
    public function generatePasskey(): string
    {
        return App::make(UuidFactory::class)->uuid4()->toString();
    }

    /**
     * Validate if the given passkey is a valid UUID.
     *
     * @param  string  $passkey  The passkey to be validated.
     * @return bool Returns true if the passkey is a valid UUID, false otherwise.
     */
    public function validatePasskey($passkey): bool
    {
        return Str::isUuid($passkey);
    }

    public function encryptData($user, $data) {}

    public function decryptData($user, $data) {}

    public function reencyptData($user)
    {
        /**
         * decryptData
         * generatePasskey
         * validatePasskey
         * encryptData
         */
    }
}
