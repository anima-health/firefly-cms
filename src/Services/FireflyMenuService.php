<?php

namespace anima\firefly\Services;

use anima\firefly\Http\models\Menu;
use anima\firefly\Http\models\Tenant;
use Illuminate\Support\Facades\Auth;

class FireflyMenuService
{
    public function getMenu()
    {
        $menuSlug = Auth::check() ? 'user-menu' : 'main-menu';
        $menu = Menu::where('slug', $menuSlug)->with('menuItems')->first();
        $menuData = [
            'name' => $menu->name ?? null,
            'items' => $menu->menuItems ?? null,
            'logo' => Tenant::current()->logo_url ?? null,
        ];

        return $menuData;
    }
}
