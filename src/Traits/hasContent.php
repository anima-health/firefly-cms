<?php

namespace anima\firefly\Traits;

use anima\firefly\Http\models\Tenant;
use Inertia\Inertia;

trait hasContent
{
    /**
     * defines content rows that can be used on page
     */
    public function defineContent()
    {
        return [
            'content' => config('firefly.content.content'),
            'hero' => config('firefly.content.hero'),
            'cards' => config('firefly.content.cards'),
            'banner' => config('firefly.content.banner'),
            'newsletter' => config('firefly.content.newsletter'),
            'table' => config('firefly.content.table'),
        ];
    }

    /**
     * gets frontend template file path to dosplay page
     */
    protected function getTemplate()
    {
        return 'Firefly::Templates/'.ucwords($this->template);
    }

    /**
     * gets frontend view
     */
    public function getView($menu = null)
    {
        return Inertia::render($this->getTemplate(), [
            'content' => json_decode($this->content),
            'menu' => [
                'name' => $menu['menu']->name ?? null,
                'items' => $menu['menu']->menuItems ?? null,
                'logo' => Tenant::current()->logo_url ?? null,
            ],
            'notifications' => $menu['notifications'] ?? null,
        ]);
    }
}
