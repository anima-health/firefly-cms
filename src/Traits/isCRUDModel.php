<?php

namespace anima\firefly\Traits;

trait isCRUDModel
{
    public static function getFillableAttributes(): array
    {
        return (new static)->getFillable();
    }
}
