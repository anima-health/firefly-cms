<?php

namespace anima\firefly\Traits;

use anima\firefly\Http\models\Tiers;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

trait hasTier
{
    public function hasTier($tier): bool
    {
        $tiers = $this->belongsToMany(
            Tiers::class,
            'model_has_tiers',
            'model_id',
            'tier_id'
        )->pluck('name');

        foreach ($tiers as $r) {
            if ($r == $tier) {
                return true;
            }
        }

        return false;
    }

    public function tiers(): BelongsToMany
    {
        return $this->belongsToMany(
            Tiers::class,
            'model_has_tiers',
            'model_id',
            'tier_id'
        );
    }

    public function syncTiers($tiers)
    {
        $tierIds = Tiers::wherein('name', $tiers)->pluck('id')->toArray();

        $tierData = [];
        foreach ($tierIds as $tierId) {
            $tierData[$tierId] = ['model_type' => get_class($this)];
        }

        $this->tiers()->sync($tierData);
    }

    public function detachTiers()
    {
        $this->tiers()->detach();
    }
}
