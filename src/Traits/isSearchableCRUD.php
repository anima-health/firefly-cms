<?php

namespace anima\firefly\Traits;

use Laravel\Scout\Searchable;

trait isSearchableCRUD
{
    use Searchable;

    /**
     * Get the name of the model.
     *
     * @return string The name of the model.
     */
    abstract public function getModelName(): string;

    /**
     * Get the admin link for the model.
     *
     * @return string The admin link for the model.
     */
    abstract public function getAdminLink(): string;

    /**
     * Get additional data related to the model.
     *
     * @return array An array of additional data.
     */
    abstract public function getAdditionalData(): array;

    /**
     * Get the name of the index associated with the model for search functionality.
     *
     * @return string The name of the search index.
     */
    public function searchableAs(): string
    {
        return 'global_search';
    }

    /**
     * Get the value used to index the model.
     */
    public function getScoutKey(): mixed
    {
        return $this->getModelName().'_'.$this->id;
    }

    /**
     * Get the key name used to index the model.
     */
    public function getScoutKeyName(): mixed
    {
        return 'id';
    }

    /**
     * Convert the model instance to an array that can be used for search indexing.
     *
     * @return array The searchable array representation of the model.
     */
    public function toSearchableArray(): array
    {
        return [
            'id' => $this->getModelName().'_'.$this->id,
            'type' => $this->getModelName(),
            'name' => $this->name,
            'link' => $this->getAdminLink(),
            'additional' => $this->getAdditionalData(),
        ];
    }
}
