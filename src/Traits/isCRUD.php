<?php

namespace anima\firefly\Traits;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Inertia\Inertia;

trait isCRUD
{
    private $items;

    protected $current;

    protected $model;

    protected $view;

    protected $data;

    private $itemsCan;

    protected $user;

    public function getAdminView($view, $data = [], $model = null)
    {
        $this->user = Auth::user();
        $this->view = $view;
        $this->data = $data;
        $this->model = $model;

        $this->getMenuItems();
        $this->getDataOptions();
        $can = [];
        if ($this->model) {
            $modelClass = $this->model;
            $modelInstance = $this->data['data']['model'];
            $can = [
                'create' => $this->user->can('create', app($modelClass)::class),
                'read' => $this->user->can('viewAny', app($modelClass)::class),
                'update' => $this->user->can('update', $modelInstance),
                'delete' => $this->user->can('delete', $modelInstance),
            ];
        }

        return $this->renderView($can);
    }

    private function renderView($can)
    {
        $notifications = $this->user->notifications->filter(function ($notification) {
            return $notification->data['isAdmin'] === true;
        });

        return Inertia::render($this->view, [
            ...$this->data,
            'can' => [
                'access_cms' => $this->user->hasPermission('Access CMS'),
                'sidePanel' => $this->itemsCan,
                ...$can,
            ],
            'user' => $this->user->select('id', 'fname', 'lname', 'email', 'profile_image')->first()->toArray(),
            'user_notifications' => $notifications,
            'recent_searches' => json_decode(Redis::get('user_'.$this->user->id.'_recent_search')),
        ]);
    }

    private function getMenuItems()
    {
        $this->items = config('firefly');
        unset($this->items['content']);
        unset($this->items['crud']);
        uasort($this->items, fn ($a, $b) => strcmp($a['section']['id'], $b['section']['id']));

        $cacheKey = 'user_'.$this->user->user_passkey.'_items_can';
        $cacheTTL = 604800; // 7 days in seconds

        if (Redis::ttl($cacheKey) < $cacheTTL && Redis::get($cacheKey)) {
            $this->itemsCan = json_decode(Redis::get($cacheKey), true);
        } else {
            $this->itemsCan = [];
            foreach ($this->items as $item) {
                if (array_key_exists('gate', $item)) {
                    $this->itemsCan[$item['name']] = $this->user->can($item['gate'], app($item['model'])::class);
                } else {
                    $this->itemsCan[$item['name']] = true;
                }
            }
            Redis::set($cacheKey, json_encode($this->itemsCan), 'EX', $cacheTTL);
        }
    }

    private function getDataOptions()
    {
        if (! str_ends_with($this->view, 'Index')) {
            if (! array_key_exists('data', $this->data)) {
                $this->data['data'] = [];
            }

            if (array_key_exists('options', $this->data['data'])) {
                $this->data['data']['options'] = array_merge(
                    $this->data['data']['options'],
                    config('firefly.crud.options.'.explode('\\', $this->model)[count(explode('\\', $this->model)) - 1]) ?? []
                );
            } else {
                $this->data['data'] = array_merge(
                    $this->data['data'],
                    [
                        'options' => config('firefly.crud.options.'.explode('\\', $this->model)[count(explode('\\', $this->model)) - 1]),
                    ]
                );
            }
        }

        $this->data = array_merge(
            $this->data,
            [
                'menu_items' => $this->items,
                'current' => $this->current,
            ]
        );
    }
}
