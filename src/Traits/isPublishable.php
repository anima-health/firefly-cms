<?php

namespace anima\firefly\Traits;

trait isPublishable
{
    /**
     * Check if the model is currently published.
     */
    public function isPublished(): bool
    {
        return $this->published === 1;
    }

    /**
     * Mark the model as published.
     *
     * @return $this
     */
    public function publish(): self
    {
        $this->published = 1;
        $this->save();

        return $this;
    }

    /**
     * Mark the model as unpublished.
     *
     * @return $this
     */
    public function unpublish(): self
    {
        $this->published = 0;
        $this->save();

        return $this;
    }

    /**
     * Scope a query to only include published models.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished($query)
    {
        return $query->where('published', 1);
    }

    /**
     * Scope a query to only include unpublished models.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUnpublished($query)
    {
        return $query->where('published', 0);
    }
}
