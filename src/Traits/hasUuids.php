<?php

namespace anima\firefly\Traits;

use Illuminate\Support\Facades\App;
use Ramsey\Uuid\UuidFactory;

/**
 * Trait for models that use Uuids. Adds a uuid when a model is saved, if it is
 * empty. Generally, this should only need to happen during creating.
 */
trait hasUuids
{
    protected static function bootHasUuids()
    {
        static::saving(function ($model) {
            if (! empty($model->uuid_column) && empty($model->{$model->uuid_column})) {
                $model->{$model->uuid_column} = App::make(UuidFactory::class)->uuid4()->toString();
            }
        });
    }
}
