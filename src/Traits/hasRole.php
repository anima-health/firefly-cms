<?php

namespace anima\firefly\Traits;

use anima\firefly\Http\models\Role;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

trait hasRole
{
    public function hasRole($role): bool
    {
        $roles = $this->belongsToMany(
            Role::class,
            'model_has_roles',
            'model_id',
            'role_id'
        )->pluck('name');

        foreach ($roles as $r) {
            if ($r == $role) {
                return true;
            }
        }

        return false;
    }

    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(
            Role::class,
            'model_has_roles',
            'model_id',
            'role_id'
        );
    }

    public function hasPermission($permission): bool
    {
        $roles = $this->belongsToMany(
            Role::class,
            'model_has_roles',
            'model_id',
            'role_id'
        )->get();

        foreach ($roles as $role) {
            if ($role->hasPermission($permission)) {
                return true;
            }
        }

        return false;
    }

    public function permissions(): Collection
    {
        $roles = $this->belongsToMany(
            Role::class,
            'model_has_roles',
            'model_id',
            'role_id'
        )->get();

        $all = new Collection;
        foreach ($roles as $role) {
            $all = $all->merge($role->permissions);
        }

        return $all;
    }

    public function isAdmin(): bool
    {
        return count($this->roles) > 0;
    }

    public function syncRoles($role)
    {
        $roleIds = Role::where('name', $role)->pluck('id')->toArray();

        $roleData = [];
        foreach ($roleIds as $roleId) {
            $roleData[$roleId] = ['model_type' => get_class($this)];
        }

        $this->roles()->sync($roleData);
    }
}
