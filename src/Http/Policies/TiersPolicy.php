<?php

namespace anima\firefly\Http\Policies;

use anima\firefly\Http\models\Tiers;
use App\Models\User;

class TiersPolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        return $user->hasPermission('Tiers Read');
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, Tiers $tiers): bool
    {
        return $user->hasPermission('Tiers Read');
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        return $user->hasPermission('Tiers Create');
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Tiers $tiers): bool
    {
        return $user->hasPermission('Tiers Edit');
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, Tiers $tiers): bool
    {
        return $user->hasPermission('Tiers Delete');
    }
}
