<?php

namespace anima\firefly\Http\Policies;

use App\Models\User;

class UsersPolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        return $user->hasPermission('Users Read');
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, User $users): bool
    {
        return $user->hasPermission('Users Read');
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        return $user->hasPermission('Users Create');
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, User $users): bool
    {
        return $user->hasPermission('Users Edit');
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, User $users): bool
    {
        return $user->hasPermission('Users Delete');
    }
}
