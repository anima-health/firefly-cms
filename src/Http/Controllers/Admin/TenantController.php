<?php

namespace anima\firefly\Http\controllers\admin;

use anima\firefly\Http\models\Tenant;
use anima\firefly\Interfaces\AdminCrud;
use anima\firefly\Traits\isCRUD;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;

class TenantController extends Controller implements AdminCrud
{
    use isCRUD;

    public function __construct()
    {
        $this->current = 'Tenants';
        $this->model = 'App\Models\Tenant';
    }

    public function index()
    {
        Gate::authorize('viewAny', Tenant::class);
        $tenants = Tenant::all();

        return $this->getAdminView('Firefly::Admin/Tenants/Index', [
            'data' => $tenants,
        ]);
    }
}
