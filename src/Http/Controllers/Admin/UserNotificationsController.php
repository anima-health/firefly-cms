<?php

namespace anima\firefly\Http\controllers\admin;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

class UserNotificationsController extends Controller
{
    public function markAllReadAdmin(): void
    {
        Auth::user()->unreadNotifications->where('data.isAdmin', true)->markAsRead();
    }

    public function deleteAdmin($id): void
    {
        foreach (Auth::user()->notifications->where('data.isAdmin', true) as $notification) {
            if ($notification->id === $id) {
                $notification->delete();
            }
        }
    }

    public function deleteAllAdmin(): void
    {
        Auth::user()->notifications()->where('data.isAdmin', true)->delete();
    }

    public function markAllReadUser(): void
    {
        Auth::user()->unreadNotifications->where('data.isAdmin', false)->markAsRead();
    }

    public function deleteUser($id): void
    {
        foreach (Auth::user()->notifications->where('data.isAdmin', false) as $notification) {
            if ($notification->id === $id) {
                $notification->delete();
            }
        }
    }

    public function deleteAllUser(): void
    {
        Auth::user()->notifications()->where('data.isAdmin', false)->delete();
    }
}
