<?php

namespace anima\firefly\Http\controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class RedisController extends Controller
{
    public function storeValue(Request $request)
    {
        $key = $request->input('key');
        $newValue = array_merge($request->input('value'), ['timestamp' => now()->timestamp]);

        $value = $this->getExistingValues($key);

        if ($this->valueExists($value, $newValue)) {
            $value = $this->removeExistingValue($value, $newValue);
        }

        $value[] = $newValue;
        $value = $this->sortAndLimitValues($value);

        Redis::set($key, json_encode($value));

        return response()->json(['status' => 'success', 'message' => 'Value stored']);
    }

    private function getExistingValues($key)
    {
        if (Redis::exists($key)) {
            return json_decode(Redis::get($key), true);
        }

        return [];
    }

    private function valueExists($value, $newValue)
    {
        return collect($value)->contains(function ($item) use ($newValue) {
            return $item['id'] === $newValue['id'] &&
                   $item['name'] === $newValue['name'] &&
                   $item['type'] === $newValue['type'];
        });
    }

    private function removeExistingValue($value, $newValue)
    {
        return collect($value)->reject(function ($item) use ($newValue) {
            return $item['id'] === $newValue['id'] &&
                   $item['name'] === $newValue['name'] &&
                   $item['type'] === $newValue['type'];
        })->toArray();
    }

    private function sortAndLimitValues($value)
    {
        usort($value, function ($a, $b) {
            return $b['timestamp'] - $a['timestamp'];
        });

        return array_slice($value, 0, 6);
    }
}
