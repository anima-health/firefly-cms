<?php

namespace anima\firefly\Http\controllers\admin;

use anima\firefly\Traits\isCRUD;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    use isCRUD;

    public function __construct()
    {
        $this->current = 'Home';
        $this->model = null;
    }

    public function index()
    {
        if (! Auth::user()->hasPermission('Access CMS')) {
            abort(403);
        }

        return $this->getAdminView('Firefly::Admin/Dashboard');
    }
}
