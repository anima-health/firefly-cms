<?php

namespace anima\firefly\Http\controllers\admin;

use anima\firefly\Events\PageEdit;
use anima\firefly\Http\models\Tenant;
use anima\firefly\Http\models\Tiers;
use anima\firefly\Http\Requests\FireflyUserRequest;
use anima\firefly\Interfaces\AdminCrud;
use anima\firefly\Traits\isCRUD;
use App\Models\User;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Str;

class UsersController extends Controller implements AdminCrud
{
    use isCRUD;

    public function __construct()
    {
        $this->current = 'Users';
        $this->model = 'App\Models\User';
    }

    public function index()
    {
        Gate::authorize('viewAny', User::class);

        $uers = User::with('roles')->with('tiers')->get();

        return $this->getAdminView('Firefly::Admin/Users/Index', [
            'data' => $uers,
            'badgeColors' => Tiers::all()->pluck('color', 'name')->toArray(),
        ]);
    }

    public function create()
    {
        Gate::authorize('create', User::class);

        $page = new user;

        return $this->getAdminView('Firefly::Admin/Users/Create', [
            'data' => [
                'model' => $page->getFillableAttributes(),
                'options' => [
                    'tier' => [
                        'options' => Tiers::pluck('name')->toArray(),
                        'default' => [Tiers::first()->name],
                    ],
                ],
            ],
        ], $this->model);
    }

    public function store(FireflyUserRequest $request)
    {
        Gate::authorize('create', User::class);

        try {
            $tenant = Tenant::current();
            $user = User::create([
                'tenant_id' => $tenant->id,
                'fname' => $request->fname,
                'lname' => $request->lname,
                'email' => $request->email,
                'profile_image' => $request->profile_image,
                'account_type' => $request->account_type,
                'gender' => $request->gender,
                'dob' => $request->dob,
                'country' => $request->country,
                'language' => $request->language,
                'business_role' => $request->business_role,
                'business_name' => $request->business_name,
                'marketing' => $request->marketing,
                'password' => bcrypt(Str::random(100)),
                'unsubcribe' => Str::random(100),
            ]);

            $user->syncTiers($request->tiers);
            $user->syncRoles($request->role);

            // Broadcast the event to Redis
            $message = 'User created successfully';
            $redis = Redis::connection();
            if ($redis->ping()) {
                event(new PageEdit($message));
            }

            // redirect to the index page
            return redirect()->route('admin.users.index');

        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }

    public function edit($id)
    {
        $user = User::where('id', $id)->with('roles')->with('tiers')->first();

        Gate::authorize('update', $user);

        return $this->getAdminView('Firefly::Admin/Users/Edit', [
            'data' => [
                'model' => $user,
                'options' => [
                    'tier' => [
                        'options' => Tiers::pluck('name')->toArray(),
                        'default' => [Tiers::first()->name],
                    ],
                ],
            ],
        ], $this->model);
    }

    public function update(FireflyUserRequest $request, $id)
    {
        $user = User::where('id', $id)->first();

        Gate::authorize('update', $user);

        $user->syncTiers($request->tiers);
        $user->syncRoles($request->role);

        $user->update([
            'fname' => $request->fname,
            'lname' => $request->lname,
            'email' => $request->email,
            'profile_image' => $request->profile_image,
            'account_type' => $request->account_type,
            'gender' => $request->gender,
            'dob' => $request->dob,
            'country' => $request->country,
            'language' => $request->language,
            'business_role' => $request->business_role,
            'business_name' => $request->business_name,
            'marketing' => $request->marketing,
        ]);

        // Broadcast the event to Redis
        $message = 'User updated successfully';
        $redis = Redis::connection();
        if ($redis->ping()) {
            event(new PageEdit($message));
        }

        return redirect()->route('admin.users.index');
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);

        Gate::authorize('delete', $user);

        $user->delete();

        $message = 'User deleted successfully';
        $redis = Redis::connection();
        if ($redis->ping()) {
            event(new PageEdit($message));
        }

        return redirect()->back()->with('success', 'User deleted successfully')->with('data', User::all());
    }
}
