<?php

namespace anima\firefly\Http\controllers\admin;

use anima\firefly\Events\PageEdit;
use anima\firefly\Http\models\Permission;
use anima\firefly\Http\models\Role;
use anima\firefly\Http\Requests\FireflyRoleRequest;
use anima\firefly\Interfaces\AdminCrud;
use anima\firefly\Traits\isCRUD;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Redis;

class RoleController extends Controller implements AdminCrud
{
    use isCRUD;

    public function __construct()
    {
        $this->current = 'Roles';
        $this->model = 'anima\firefly\Http\models\Role';
    }

    public function index()
    {
        Gate::authorize('viewAny', Role::class);

        $roles = Role::withCount('permissions')->get();

        return $this->getAdminView('Firefly::Admin/Roles/Index', ['data' => $roles]);
    }

    public function create()
    {
        Gate::authorize('create', Role::class);

        $role = new Role;

        return $this->getAdminView('Firefly::Admin/Roles/Create', [
            'data' => [
                'model' => $role->getFillableAttributes(),
                'options' => [
                    'permissions' => [
                        'options' => Permission::pluck('name')->toArray(),
                        'default' => ['Access CMS'],
                    ],
                ],
            ],
        ], $this->model);
    }

    public function store(FireflyRoleRequest $request)
    {
        Gate::authorize('create', Role::class);

        $role = Role::create($request->all());

        // Prepare an array to hold permission names
        $permissionsToAttach = [];

        // Iterate through the permissions array
        foreach ($request->permissions as $category => $actions) {
            foreach ($actions as $action) {
                // Construct the permission name
                if ($action === 'Access CMS') {
                    $permissionName = "{$category}";
                } else {
                    $permissionName = "{$category} {$action}";
                }

                // Check if the permission exists in the database & add the permission id to the array
                if (Permission::where('name', $permissionName)->exists()) {
                    $permissionsToAttach[] = Permission::where('name', $permissionName)->first()->id;
                }
            }
        }

        $role->permissions()->detach();
        // dd($request->permissions, $permissionsToAttach);
        $role->permissions()->attach($permissionsToAttach);

        // Broadcast the event to Redis
        $message = 'Role created successfully';
        $redis = Redis::connection();
        if ($redis->ping()) {
            event(new PageEdit($message));
        }

        return redirect()->route('admin.roles.index')->with('success', 'Role created successfully');
    }

    public function edit($id)
    {
        $role = Role::with('permissions')->findOrFail($id);

        Gate::authorize('update', $role);

        return $this->getAdminView('Firefly::Admin/Roles/Edit', [
            'data' => [
                'model' => $role,
                'options' => [
                    'permissions' => [
                        'options' => Permission::pluck('name')->toArray(),
                        'default' => ['Access CMS'],
                    ],
                ],
            ],
        ], $this->model);
    }

    public function update(FireflyRoleRequest $request, Role $role)
    {
        Gate::authorize('update', $role);

        // Prepare an array to hold permission names
        $permissionsToAttach = [];

        // Iterate through the permissions array
        foreach ($request->permissions as $category => $actions) {
            foreach ($actions as $action) {
                // Construct the permission name
                if ($action === 'Access CMS') {
                    $permissionName = "{$category}";
                } else {
                    $permissionName = "{$category} {$action}";
                }

                // Check if the permission exists in the database & add the permission id to the array
                if (Permission::where('name', $permissionName)->exists()) {
                    $permissionsToAttach[] = Permission::where('name', $permissionName)->first()->id;
                }
            }
        }

        $role->permissions()->detach();
        // dd($request->permissions, $permissionsToAttach);
        $role->permissions()->attach($permissionsToAttach);

        $role->update($request->all());

        // Broadcast the event to Redis
        $message = 'Role updated successfully';
        $redis = Redis::connection();
        if ($redis->ping()) {
            event(new PageEdit($message));
        }

        return redirect()->route('admin.roles.index')->with('success', 'Role updated successfully');
    }

    public function destroy($id)
    {
        $role = Role::findOrFail($id);

        Gate::authorize('delete', $role);

        $role->delete();

        // Broadcast the event to Redis
        $message = 'Role Deleted successfully';
        $redis = Redis::connection();
        if ($redis->ping()) {
            event(new PageEdit($message));
        }

        return redirect()->route('admin.roles.index')->with('success', 'Role deleted successfully');
    }
}
