<?php

namespace anima\firefly\Http\controllers\admin;

use anima\firefly\Events\PageEdit;
use anima\firefly\Http\models\Tiers;
use anima\firefly\Http\Requests\FireflyTierRequest;
use anima\firefly\Interfaces\AdminCrud;
use anima\firefly\Traits\isCRUD;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

class TiersController extends Controller implements AdminCrud
{
    use isCRUD;

    public function __construct()
    {
        $this->current = 'Tiers';
        $this->model = 'anima\firefly\Http\models\Tiers';
    }

    public function index()
    {
        Gate::authorize('viewAny', Tiers::class);

        $tiers = Tiers::all();

        return $this->getAdminView('Firefly::Admin/Tiers/Index', ['data' => $tiers]);
    }

    public function create()
    {
        Gate::authorize('create', Tiers::class);

        $tier = new Tiers;

        return $this->getAdminView('Firefly::Admin/Tiers/Create', [
            'data' => [
                'model' => $tier->getFillableAttributes(),
            ],
        ], $this->model);
    }

    public function store(FireflyTierRequest $request)
    {
        Gate::authorize('create', Tiers::class);

        try {
            Tiers::create($request->all());

            // Broadcast the event to Redis
            $message = 'Tier created successfully';
            $redis = Redis::connection();
            if ($redis->ping()) {
                event(new PageEdit($message));
            }

            // redirect to the index page
            return redirect()->route('admin.tiers.index');

        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }

    public function edit($id)
    {
        $tier = Tiers::where('id', $id)->first();

        Gate::authorize('update', $tier);

        return $this->getAdminView('Firefly::Admin/Tiers/Edit', [
            'data' => [
                'model' => $tier,
            ],
        ], $this->model);
    }

    public function update(FireflyTierRequest $request, $id)
    {
        $tier = Tiers::where('id', $id)->first();

        Gate::authorize('update', $tier);

        $tier->update($request->all());

        // Broadcast the event to Redis
        $message = 'Tier updated successfully';
        $redis = Redis::connection();
        if ($redis->ping()) {
            event(new PageEdit($message));
        }

        return redirect()->route('admin.tiers.index');
    }

    public function destroy($id)
    {
        $tier = Tiers::findOrFail($id);

        Gate::authorize('delete', $tier);

        $tier->delete();

        $message = 'Tier deleted successfully';
        $redis = Redis::connection();
        if ($redis->ping()) {
            event(new PageEdit($message));
        }

        return redirect()->back()->with('success', 'Tier deleted successfully')->with('data', Tiers::all());
    }
}
