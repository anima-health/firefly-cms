<?php

namespace anima\firefly\Http\controllers\admin;

use anima\firefly\Http\models\Role;
use anima\firefly\Http\models\Tenant;
use anima\firefly\Interfaces\AdminCrud;
use anima\firefly\Traits\isCRUD;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;

class SettingController extends Controller implements AdminCrud
{
    use isCRUD;

    public function __construct()
    {
        $this->current = 'Settings';
        $this->model = null;
    }

    public function index()
    {
        // Gate::authorize('viewAny', Role::class);

        $tenantSettings = Tenant::current()->getBranding();

        return $this->getAdminView('Firefly::Admin/Settings/Index', compact('tenantSettings'));
    }

    public function store(Request $request)
    {
        $tenant = Tenant::current();

        $tenant->update([
            ...$request->settings,
        ]);

        $tenant->save();

        return back();
    }
}
