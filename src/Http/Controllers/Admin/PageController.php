<?php

namespace anima\firefly\Http\controllers\admin;

use anima\firefly\Events\PageEdit;
use anima\firefly\Http\models\Tenant;
use anima\firefly\Http\models\Tiers;
use anima\firefly\Http\Requests\FireflyPageRequest;
use anima\firefly\Interfaces\AdminCrud;
use anima\firefly\Traits\isCRUD;
use App\Models\Page;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

class PageController extends Controller implements AdminCrud
{
    use isCRUD;

    public function __construct()
    {
        $this->current = 'Pages';
        $this->model = 'App\Models\Page';
    }

    public function index()
    {
        Gate::authorize('viewAny', Page::class);

        $pages = Page::all();

        return $this->getAdminView('Firefly::Admin/Pages/Index', ['data' => $pages]);
    }

    public function create()
    {
        Gate::authorize('create', Page::class);

        $page = new Page;

        return $this->getAdminView('Firefly::Admin/Pages/Create', [
            'data' => [
                'model' => $page->getFillableAttributes(),
                'content_types' => $page->defineContent(),
                'options' => [
                    'tier' => [
                        'options' => Tiers::pluck('name')->toArray(),
                        'default' => [Tiers::first()->name],
                    ],
                ],
            ],
        ], $this->model);
    }

    public function store(FireflyPageRequest $request)
    {
        Gate::authorize('create', Page::class);

        try {
            $tenant = Tenant::current();
            $page = Page::create([
                'title' => $request->title,
                'tenant_id' => $tenant->id,
                'slug' => $request->slug,
                'template' => $request->template,
                'content' => json_encode($request->content),
                'published' => $request->publish,
                'published_at' => $request->publish_at,
            ]);

            $page->syncTiers($request->tiers);

            // Broadcast the event to Redis
            $message = 'Page created successfully';
            $redis = Redis::connection();
            if ($redis->ping()) {
                event(new PageEdit($message));
            }

            // redirect to the index page
            return redirect()->route('admin.pages.index');

        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }

    public function edit($id)
    {
        $page = Page::where('id', $id)->with('tiers')->with('content')->first();

        Gate::authorize('update', $page);

        return $this->getAdminView('Firefly::Admin/Pages/Edit', [
            'data' => [
                'model' => $page,
                'content_types' => $page->defineContent(),
                'options' => [
                    'tier' => [
                        'options' => Tiers::pluck('name')->toArray(),
                        'default' => [Tiers::first()->name],
                    ],
                ],
            ],
        ], $this->model);
    }

    public function update(FireflyPageRequest $request, $id)
    {
        $page = Page::where('id', $id)->first();

        Gate::authorize('update', $page);

        $page->syncTiers($request->tiers);

        $page->update([
            'title' => $request->title,
            'slug' => $request->slug,
            'template' => $request->template,
        ]);

        foreach ($request->content as $key => $content) {
            $page->content()->updateOrCreate(
                ['id' => $content['id']], // Use 'id' only for updating existing records
                [
                    'content' => $content['content'],
                    'order' => $key,
                    'type' => $content['type'],
                ]
            );
        }

        if ($request->publish) {
            $page->publish();
            $message = 'Page saved and published successfully';
        } else {
            $page->unpublish();
            $message = 'Page saved as draft successfully';
        }

        $page->save();

        // Broadcast the event to Redis
        $redis = Redis::connection();
        if ($redis->ping()) {
            event(new PageEdit($message));
        }

        return redirect()->route('admin.pages.index');
    }

    public function destroy($id)
    {
        $page = Page::findOrFail($id);

        Gate::authorize('delete', $page);

        $page->detachTiers();
        $page->delete();

        $message = 'Page deleted successfully';
        $redis = Redis::connection();
        if ($redis->ping()) {
            event(new PageEdit($message));
        }

        return redirect()->back()->with('success', 'Page deleted successfully')->with('data', Page::all());
    }

    public function publish($id)
    {
        $page = Page::findOrFail($id);

        if ($page->isPublished()) {
            $page->unpublish();
            $message = 'Page unpublished successfully';
        } else {
            $page->publish();
            $message = 'Page published successfully';
        }

        // Broadcast the event to Redis
        $redis = Redis::connection();
        if ($redis->ping()) {
            event(new PageEdit($message));
        }

        return redirect()->back()->with('success', $message)->with('data', Page::all());
    }
}
