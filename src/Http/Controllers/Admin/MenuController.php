<?php

namespace anima\firefly\Http\controllers\admin;

use anima\firefly\Events\PageEdit;
use anima\firefly\Http\models\Menu;
use anima\firefly\Http\models\Tenant;
use anima\firefly\Http\Requests\FireflyMenuRequest;
use anima\firefly\Interfaces\AdminCrud;
use anima\firefly\Traits\isCRUD;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Str;

class MenuController extends Controller implements AdminCrud
{
    use isCRUD;

    public function __construct()
    {
        $this->current = 'Menus';
        $this->model = 'anima\firefly\Http\models\Menu';
    }

    public function index()
    {
        Gate::authorize('viewAny', Menu::class);
        $menus = Menu::withcount('menuItems')->get();

        return $this->getAdminView('Firefly::Admin/Menus/Index', [
            'data' => $menus,
        ]);
    }

    public function create()
    {
        Gate::authorize('create', Menu::class);

        $menu = new Menu;

        return $this->getAdminView('Firefly::Admin/Menus/Create', [
            'data' => [
                'model' => $menu->getFillableAttributes(),
            ],
        ], $this->model);
    }

    public function store(FireflyMenuRequest $request)
    {
        Gate::authorize('create', Menu::class);

        $menu = Menu::create([
            'name' => $request->name,
            'tenant_id' => Tenant::current()->id,
            'slug' => Str::slug($request->name),
        ]);

        // Create menu items from request items
        $menuItems = collect($request->items)->map(function ($item, $index) {
            return [
                'name' => $item['name'],
                'url' => $item['url'],
                'target' => $item['target'],
                'order' => $index, // Maintain the order using the array index
            ];
        });

        // Create the menu items
        $menu->menuItems()->createMany($menuItems->toArray());

        // Broadcast the event to Redis
        $message = 'Menu created successfully';
        $redis = Redis::connection();
        if ($redis->ping()) {
            event(new PageEdit($message));
        }

        // redirect to the index page
        return redirect()->route('admin.menus.index');
    }

    public function edit(Menu $menu)
    {
        Gate::authorize('update', $menu);

        $menu->load(['menuItems' => fn ($query) => $query->orderBy('order', 'asc')]);

        return $this->getAdminView('Firefly::Admin/Menus/Edit', [
            'data' => [
                'model' => $menu,
            ],
        ], $this->model);
    }

    public function update(FireflyMenuRequest $request, $id)
    {
        $menu = Menu::with('menuItems')->where('id', $id)->first();

        Gate::authorize('update', $menu);

        $menu->update($request->all());

        // Keep track of processed IDs
        $processedIds = [];

        // Loop through the incoming items and update or create them
        foreach ($request->items as $index => $item) {
            $menuItem = $menu->menuItems()->updateOrCreate(
                ['id' => $item['id'] ?? null], // Match by ID for updates, or create new
                [
                    'name' => $item['name'],
                    'url' => $item['url'],
                    'target' => $item['target'],
                    'order' => $index, // Maintain the correct order
                ]
            );

            // Add the ID to the list of processed items
            $processedIds[] = $menuItem->id;
        }

        $menu->menuItems()
            ->whereNotIn('id', $processedIds)
            ->delete();

        // Broadcast the event to Redis
        $message = 'Menu updated successfully';
        $redis = Redis::connection();
        if ($redis->ping()) {
            event(new PageEdit($message));
        }

        return redirect()->route('admin.menus.index');
    }

    public function destroy($id)
    {
        $menu = Menu::findOrFail($id);

        Gate::authorize('delete', $menu);

        $menu->delete();

        $message = 'Menu deleted successfully';
        $redis = Redis::connection();
        if ($redis->ping()) {
            event(new PageEdit($message));
        }

        return redirect()->back()->with('success', 'Menu deleted successfully')->with('data', Menu::all());
    }
}
