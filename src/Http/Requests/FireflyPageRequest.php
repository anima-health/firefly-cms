<?php

namespace anima\firefly\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FireflyPageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'title' => 'required|string',
            'slug' => 'required|string|unique:content.pages,slug,'.$this->page,
            'publish' => 'required|boolean',
            'template' => 'required|string',
            'tiers' => 'required|array',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'title.required' => 'A title is required',
            'template.required' => 'A page template is required',
            'tiers.required' => 'The tiers field is required.',
        ];
    }
}
