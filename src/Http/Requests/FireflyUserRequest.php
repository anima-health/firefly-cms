<?php

namespace anima\firefly\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FireflyUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'fname' => 'required|string',
            'lname' => 'required|string',
            'email' => 'required|string|email|unique:users,email,'.$this->user,
            'profile_image' => 'image|nullable',
            'account_type' => 'required|string',
            'gender' => 'required|string',
            'dob' => 'required|date_format:Y-m-d|before:18 years ago',
            'country' => 'required|string',
            'language' => 'required|string',
            'business_role' => 'string|nullable',
            'business_name' => 'string|nullable',
            'marketing' => 'required|string',
            'role' => 'required|string',
            'tiers' => 'required|array',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'fname.required' => 'The first name field is required.',
            'lname.required' => 'The last name field is required.',
            'email.required' => 'The email field is required.',
            'email.email' => 'The email field must be a valid email address.',
            'email.unique' => 'The email field must be unique.',
            'profile_image.image' => 'The profile image field must be an image.',
            'account_type.required' => 'The account type field is required.',
            'gender.required' => 'The gender field is required.',
            'dob.required' => 'The date of birth field is required.',
            'dob.date_format' => 'The date of birth field must be in the format of YYYY-MM-DD.',
            'dob.before' => 'The date of birth field must be before 18 years ago.',
            'country.required' => 'The country field is required.',
            'language.required' => 'The language field is required.',
            'marketing.required' => 'The marketing field is required.',
            'role.required' => 'The role field is required.',
            'tiers.required' => 'The tiers field is required.',
        ];
    }
}
