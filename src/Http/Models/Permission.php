<?php

namespace anima\firefly\Http\models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Permission extends Model
{
    protected $connection = 'mysql';

    /**
     * A permission may belong to various roles
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(
            Role::class,
            'role_has_permissions',
            'permission_id',
            'role_id'
        );
    }

    /**
     * A permission may belong to a given role
     */
    public function belongsToRole($role): bool
    {
        $roles = $this->roles()->get();

        foreach ($roles as $r) {
            if ($r->name === $role) {
                return true;
            }
        }

        return false;
    }

    /**
     * A permission belongs to some users of the model associated with its guard.
     */
    public function users(): Collection
    {
        $roles = $this->roles()->get();

        $all = new Collection;
        foreach ($roles as $role) {
            $all = $all->merge($role->users);
        }

        return $all;
    }
}
