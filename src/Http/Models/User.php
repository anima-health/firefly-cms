<?php

namespace anima\firefly\Http\models;

use anima\firefly\Traits\hasRole;
use anima\firefly\Traits\hasTier;
use anima\firefly\Traits\hasUuids;
use anima\firefly\Traits\isCRUDModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, hasRole, hasTier, hasUuids, isCRUDModel, Notifiable;

    protected $connection = 'mysql';

    protected $uuid_column = 'user_passkey';

    protected $hidden = ['password', 'remember_token'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'dob' => 'date',
        'weight' => 'float',
    ];

    protected $attributes = [
        'weight' => '1.2',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'dob',
        'email',
        'gender',
        'fname',
        'lname',
        'password',
        'remember_token',
        'weight',
        'country',
        'language',
        'account_type',
        'business_name',
        'business_role',
        'profile_image',
        'marketing',
        'unsubcribe',
        'tenant_id',
        'user_passkey',
        'privacy_policy_version',
    ];
}
