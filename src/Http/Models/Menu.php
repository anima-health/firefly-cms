<?php

namespace anima\firefly\Http\models;

use anima\firefly\Traits\isCRUDModel;
use anima\firefly\Traits\isSearchableCRUD;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Menu extends Model
{
    use isCRUDModel;
    use isSearchableCRUD;

    protected $connection = 'content';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
    ];

    public function menuItems(): HasMany
    {
        return $this->hasMany(MenuItem::class);
    }

    public function getModelName(): string
    {
        return 'menu';
    }

    public function getAdminLink(): string
    {
        return route('admin.menus.edit', $this->id);
    }

    public function getAdditionalData(): array
    {
        return [];
    }
}
