<?php

namespace anima\firefly\Http\models;

use anima\firefly\Http\models\Scopes\TenantScope;
use anima\firefly\Traits\hasContent;
use anima\firefly\Traits\hasTier;
use anima\firefly\Traits\isCRUDModel;
use anima\firefly\Traits\isPublishable;
use anima\firefly\Traits\isSearchableCRUD;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use hasContent { defineContent as protected defineContentTrait; }
    use HasFactory;
    use hasTier;
    use isCRUDModel;
    use isPublishable;
    use isSearchableCRUD;

    protected $connection = 'content';

    protected $fillable = [
        'title',
        'tenant_id',
        'slug',
        'published',
        'published_at',
        'template',
    ];

    protected static function booted()
    {
        static::addGlobalScope(new TenantScope);
    }

    // Scope to find pages ready to be published
    public function scopeReadyToPublish($query)
    {
        return $query->select('id', 'title', 'tenant_id', 'published', 'published_at')
            ->where('published', false)
            ->whereNotNull('published_at')
            ->where('published_at', '<=', now());
    }

    public function content()
    {
        return $this->hasMany(PageContent::class)->orderBy('order');
    }

    public function getModelName(): string
    {
        return 'page';
    }

    public function getAdminLink(): string
    {
        return route('admin.roles.edit', $this->id);
    }

    public function getAdditionalData(): array
    {
        // Get related tiers
        $tiers = $this->tiers->map(function ($tier) {
            return [
                'id' => 'tier'.$this->id,
                'type' => 'tier',
                'name' => $tier->name,
            ];
        });

        return [
            'tiers' => $tiers,
        ];
    }
}
