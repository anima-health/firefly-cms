<?php

namespace anima\firefly\Http\models;

use anima\firefly\Traits\isCRUDModel;
use anima\firefly\Traits\isSearchableCRUD;
use Illuminate\Database\Eloquent\Model;

class Tiers extends Model
{
    use isCRUDModel;
    use isSearchableCRUD;

    protected $connection = 'mysql';

    protected $fillable = [
        'name',
        'color',
    ];

    public function getModelName(): string
    {
        return 'tier';
    }

    public function getAdminLink(): string
    {
        return route('admin.tiers.edit', $this->id);
    }

    public function getAdditionalData(): array
    {
        return [];
    }
}
