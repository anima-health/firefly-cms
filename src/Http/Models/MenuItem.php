<?php

namespace anima\firefly\Http\models;

use anima\firefly\Traits\isSearchableCRUD;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class MenuItem extends Model
{
    use isSearchableCRUD;

    protected $connection = 'content';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'url',
        'target',
        'order',
        'menu_id',
    ];

    public function menu(): BelongsTo
    {
        return $this->belongsTo(Menu::class);
    }

    public function getModelName(): string
    {
        return 'menu item';
    }

    public function getAdminLink(): string
    {
        return route('admin.menus.edit', $this->menu->id);
    }

    public function getAdditionalData(): array
    {
        return [
            'menu' => $this->menu->name, // Add the related items to the menu search
        ];
    }

    /**
     * Get custom value used to index the model.
     */
    public function getScoutKey(): mixed
    {
        $menu = $this->menu;

        return 'menuitem'.$this->id.'_'.$menu->id;
    }
}
