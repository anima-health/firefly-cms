<?php

namespace anima\firefly\Http\models;

use anima\firefly\Traits\isCRUDModel;
use Spatie\Multitenancy\Models\Tenant as BaseTenant;

class Tenant extends BaseTenant
{
    use isCRUDModel;

    protected $connection = 'mysql';

    protected $fillable = ['id', 'name', 'domain', 'subdomain', 'logo_url', 'primary_color', 'secondary_color'];

    public function getBranding()
    {
        return [
            'logo_url' => $this->logo_url,
            'primary_color' => $this->primary_color,
            'secondary_color' => $this->secondary_color,
        ];
    }
}
