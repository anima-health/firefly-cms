<?php

namespace anima\firefly\Http\models;

use Illuminate\Database\Eloquent\Model;

class PageContent extends Model
{
    protected $connection = 'content';

    protected $fillable = [
        'id',
        'content',
        'order',
        'type',
    ];

    protected $casts = [
        'content' => 'array',
        'id' => 'string',
    ];

    public function page()
    {
        return $this->belongsTo(Page::class);
    }
}
