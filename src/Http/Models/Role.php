<?php

namespace anima\firefly\Http\models;

use anima\firefly\Traits\isCRUDModel;
use anima\firefly\Traits\isSearchableCRUD;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Role extends Model
{
    use isCRUDModel;
    use isSearchableCRUD;

    protected $connection = 'mysql';

    protected $fillable = [
        'name',
    ];

    /**
     * A role may be given various permissions.
     */
    public function permissions(): BelongsToMany
    {
        return $this->belongsToMany(
            Permission::class,
            'role_has_permissions',
            'role_id',
            'permission_id'
        );
    }

    /**
     * A role may have a given permission
     */
    public function hasPermission($permission): bool
    {
        $permissions = $this->permissions()->get();

        foreach ($permissions as $p) {
            if ($p->name === $permission) {
                return true;
            }
        }

        return false;
    }

    /**
     * A role belongs to some users of the model associated with its guard.
     */
    public function users(): BelongsToMany
    {
        return $this->morphedByMany(
            User::class,
            'model',
            'model_has_roles'
        );
    }

    public function getModelName(): string
    {
        return 'role';
    }

    public function getAdminLink(): string
    {
        return route('admin.roles.edit', $this->id);
    }

    public function getAdditionalData(): array
    {
        return [];
    }
}
