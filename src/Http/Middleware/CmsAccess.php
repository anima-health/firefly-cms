<?php

namespace anima\firefly\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CmsAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {

        if (! $request->user() || ! $request->user()->hasPermission('Access CMS')) {
            return redirect('/');
        }

        return $next($request);
    }
}
