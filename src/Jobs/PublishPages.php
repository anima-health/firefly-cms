<?php

namespace anima\firefly\Jobs;

use anima\firefly\Notifications\PagePublished;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Queue\Queueable;

class PublishPages implements ShouldQueue
{
    use Queueable;

    protected $pages;

    /**
     * Create a new job instance.
     */
    // take collection of pages
    public function __construct(Collection $pages)
    {
        $this->pages = $pages;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $adminUsers = User::whereHas('roles')->get();

        foreach ($this->pages as $page) {
            $page->publish();
            $page->save();

            foreach ($adminUsers as $admin) {
                $admin->notify(new PagePublished($page->title));
            }
        }
    }
}
