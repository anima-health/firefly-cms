<?php

namespace anima\firefly\database\seeders;

use anima\firefly\Http\models\Menu;
use anima\firefly\Http\models\MenuItem;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menus = [
            'main menu' => [
                'About',
                'Features',
                'Who Can We Help?',
            ],
            'user menu' => [
                'Assessments',
                'Health Checks',
                'Journals',
                'Stats',
            ],
        ];

        foreach ($menus as $menu => $items) {
            $menu = Menu::updateOrCreate([
                'name' => $menu,
                'slug' => Str::slug($menu),
            ]);

            $current_menu_id = $menu->id;

            $idx = 1;
            foreach ($items as $item) {
                MenuItem::updateOrCreate([
                    'name' => $item,
                    'url' => '/'.Str::slug($item),
                    'target' => '_self',
                    'order' => $idx,
                    'menu_id' => $current_menu_id,
                ]);
                $idx++;
            }
        }

    }
}
