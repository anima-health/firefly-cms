<?php

namespace anima\firefly\database\seeders;

use anima\firefly\Facades\Passkey;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        User::create([
            'email' => 'firefly@animahub.com',
            'fname' => 'firefly',
            'lname' => 'Superuser',
            'password' => bcrypt('changeme'),
            'account_type' => 'Personal',
            'dob' => Carbon::now()->subYears(18),
            'unsubcribe' => Str::random(100),
            'user_passkey' => Passkey::generatePasskey(),
            'privacy_policy_version' => '1.0.0',
        ]);

    }
}
