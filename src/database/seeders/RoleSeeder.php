<?php

namespace anima\firefly\database\seeders;

use anima\firefly\Http\models\Permission;
use anima\firefly\Http\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->permissions();
        $this->roles();
        $this->role_has_permissions();
        $this->set_user_roles();
    }

    private function roles()
    {
        $roles = [
            'Landlord Super User',
            'Landlord Admin',
            'Landlord Tester',
            'Tenant Super User',
            'Tenant Admin',
            'Tenant Tester',
        ];

        foreach ($roles as $role) {
            Role::firstOrCreate([
                'name' => $role,
            ]);
        }
    }

    private function permissions()
    {
        $csvFile = fopen(base_path('vendor/anima/firefly/database/data/permissions.csv'), 'r');

        $firstline = true;
        while (($data = fgetcsv($csvFile, 2000, ',')) !== false) {
            if (! $firstline) {
                Permission::firstOrCreate([
                    'name' => $data['0'],
                ]);
            }
            $firstline = false;
        }

        fclose($csvFile);
    }

    private function role_has_permissions()
    {
        $file = fopen(base_path('vendor/anima/firefly/database/data/role_permission.csv'), 'r');
        $header = true;

        while (($row = fgetcsv($file)) !== false) {
            if ($header) {
                $header = false;

                continue;
            }

            DB::table('role_has_permissions')->updateOrInsert([
                'role_id' => $row[0],
                'permission_id' => $row[1],
            ]);
        }

        fclose($file);
    }

    private function set_user_roles()
    {
        DB::table('model_has_roles')->updateOrInsert([
            'role_id' => '1',
            'model_type' => 'App/Model/User',
            'model_id' => '1',
        ]);
    }
}
