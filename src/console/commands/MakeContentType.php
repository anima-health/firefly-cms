<?php

namespace anima\firefly\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;

class MakeContentType extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:contentType {name} {--f|force : Overwrite existing files}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new content type';

    protected $name;

    protected $name_lower;

    /**
     * The Filesystem
     *
     * @var Filesystem
     */
    protected $files;

    /**
     * @var Composer
     */
    protected $composer;

    /**
     * Create a new command instance
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
        $this->composer = app()['composer'];
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->name = $this->argument('name');

        if (! $this->name) {
            return $this->error('You must provide a name. The name should be singular.');
        }

        $this->name = Str::studly($this->name);
        $this->name_lower = strtolower($this->name);

        $this->makeBlueprint();
        $this->makeVueComponent();

        $this->info('Dumping autoload...');
        $this->composer->dumpAutoloads();

        $this->line('');
        $this->info('Almost done!');

        $this->line('');
        $this->info('Add vue component import to resources/js/vendor/firefly/Content/contentArea.vue');
        $this->line('');
        $this->line(
            '    import '.$this->name." from './".$this->name.".vue'"
        );
        $this->line('');

        $this->line('');
        $this->info('Add new content type to the defineContent function in app/Models/Page.php');
        $this->line('');
        $this->line(
            "    '".$this->name_lower."' => config('firefly.content.".$this->name_lower."'),"
        );
        $this->line('');
    }

    protected function makeBlueprint()
    {
        $this->info('Creating blueprint...');

        $stub = file_get_contents(__DIR__.'/../../../installable/Content_stubs/Blueprint.stub');

        $stub = str_replace('{{name_singular}}', $this->name, $stub);
        $stub = str_replace('{{name_singular_lower}}', $this->name_lower, $stub);

        $path = base_path("config/firefly/content/{$this->name_lower}.php");

        if ($this->files->exists($path) && ! $this->option('force')) {
            return $this->error('The content type already exists. Use --force to overwrite it.');
        }

        $this->files->put($path, $stub);

        $this->info('Blueprint created successfully.');
    }

    protected function makeVueComponent()
    {
        $this->info('Creating Vue component...');

        $stub = file_get_contents(__DIR__.'/../../../installable/Content_stubs/Component.stub');

        $path = base_path("resources/js/vendor/firefly/Content/{$this->name}.vue");

        if ($this->files->exists($path) && ! $this->option('force')) {
            return $this->error('The Vue component already exists. Use --force to overwrite it.');
        }

        $this->files->put($path, $stub);

        $this->info('Vue component created successfully.');
    }
}
