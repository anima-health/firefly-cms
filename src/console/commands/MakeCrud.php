<?php

namespace anima\firefly\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;

use function Laravel\Prompts\multiselect;
use function Laravel\Prompts\select;
use function Laravel\Prompts\text;

/**
 * Artisan command for generating an Ensō CRUD config file
 */
class MakeCrud extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:crud {name?} {--f|force : Overwrite existing files}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a CRUD';

    /**
     * The name for CRUD config
     * Should be singular, Capitalized
     *
     * @var string
     */
    protected $name;

    protected $name_plural;

    protected $name_lower;

    protected $name_plural_lower;

    protected $slug;

    protected $connection;

    /**
     * The Filesystem
     *
     * @var Filesystem
     */
    protected $files;

    /**
     * @var Composer
     */
    protected $composer;

    /**
     * Create a new command instance
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
        $this->composer = app()['composer'];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->name = $this->argument('name') ?? null;

        if (! $this->name) {
            $this->name = text('What is the name of the CRUD?');
        }

        $this->name = Str::studly($this->name);
        $this->name_plural = Str::plural($this->name);
        $this->name_lower = strtolower($this->name);
        $this->name_plural_lower = strtolower($this->name_plural);
        $this->slug = Str::slug($this->name_plural);
        $this->connection = 'mysql';

        $parts = multiselect(
            label: 'What parts of the CRUD do you want to generate?',
            options: [
                'Config',
                'Controller',
                'FormRequest',
                'Migration',
                'Model',
                'Policy',
                'View',
            ]
        );

        if (in_array('Migration', $parts) || in_array('Model', $parts)) {
            $connection = select(
                label: 'What connection do you want to use?',
                options: [
                    'mysql',
                    'content',
                ],
                required: true
            );

            $this->connection = $connection;
        }

        $methods = [
            'Config' => 'makeConfigEntry',
            'Controller' => 'makeController',
            'FormRequest' => 'makeFormRequest',
            'Policy' => 'makePolicy',
            'View' => 'makeIndex',
            'Migration' => 'makeMigration',
            'Model' => 'makeModel',
        ];

        $modelAndMigrationCreated = false;

        foreach ($parts as $part) {
            if ($part === 'Migration' || $part === 'Model') {
                if (in_array('Migration', $parts) && in_array('Model', $parts)) {
                    if (! $modelAndMigrationCreated) {
                        $this->makeModelAndMigration();
                        $modelAndMigrationCreated = true;
                    }

                    continue;
                }
            } else {
                $this->{$methods[$part]}();
            }
        }

        $this->info('Dumping autoload...');
        $this->composer->dumpAutoloads();

        if (in_array('Controller', $parts) || in_array('Policy', $parts) || in_array('Model', $parts) || in_array('Migration', $parts) || in_array('Config', $parts)) {
            $this->line('');
            $this->info('Almost done!');
        }

        if (in_array('Controller', $parts)) {
            $this->line('');
            $this->info('Add route to web & admin group in routes/web.php');
            $this->line('');
            $this->line(
                "    Route::resource('admin/".$this->name_plural_lower."', App\Http\Controllers\Admin\\".$this->name.'Controller::class);'
            );
        }

        if (in_array('Policy', $parts)) {
            $this->line('');
            $this->info('Add policy to app/Providers/AppServiceProvider.php');
            $this->line('');

            $this->line(
                '   Gate::policy('.$this->name.'::class, '.$this->name.'Policy::class);'
            );

            $this->line('');
            $this->info('Create permissions in the database for this CRUD and attach them to a Role');
            $this->info('and then update the policy file app/Policies/'.$this->name.'Policy.php');
        }

        if (in_array('Model', $parts)) {
            $this->line('');
            $this->info('Add the connection to the model');
            $this->line('');
            if ($this->connection === 'mysql') {
                $this->line(
                    "    protected \$connection = 'mysql';"
                );
            } else {
                $this->line(
                    "    protected \$connection = 'content';"
                );
            }

            $this->line('');
            $this->info('Add the following trait to the model');
            $this->line('');
            $this->line(
                "    use anima\\firefly\Traits\isCRUDModel;"
            );
        }

        if (in_array('Migration', $parts)) {
            $this->line('');
            $this->info('Make sure the migration is using the correct connection.');
            $this->info('You are using the '.$this->connection.' connection. Please add the following to the migration:');
            if ($this->connection === 'mysql') {
                $this->line(
                    "    protected \$connection = 'mysql';"
                );
            } else {
                $this->line(
                    "    protected \$connection = 'content';"
                );
            }
        }

        if (in_array('Config', $parts)) {
            $this->line('');
            $this->info('Update the section details in the config file, by default it is set to "Site"');
        }

        $this->line('');
    }

    /**
     * Build the directory for the class if necessary.
     *
     * @param  string  $path
     * @return string
     */
    protected function makeDirectory($path)
    {
        if (! $this->files->isDirectory($path)) {
            $this->files->makeDirectory($path, 0777, true, true);
        }

        return $path;
    }

    protected function makeIndex(): void
    {
        $admin_path = base_path('/resources/js/Pages/Admin/');
        $model_view_path = base_path('/resources/js/Pages/Admin/'.$this->name_plural);
        $index_path = $this->getAdminViewPath();

        $this->info('Creating index view for '.$this->name.' - '.$index_path);

        if ($this->files->exists($index_path) && ! $this->option('force')) {
            $this->error('Index view file ('.$index_path.') already exists. Skipping.');
            $this->line('');

            return;
        }

        $this->makeDirectory($admin_path);
        $this->makeDirectory($model_view_path);
        $this->files->put($index_path, $this->compileIndexView());
        $this->line('');
    }

    protected function getAdminViewPath(): string
    {
        return base_path('/resources/js/Pages/Admin/'.$this->name_plural.'/Index.vue');
    }

    protected function compileIndexView(): string
    {
        $stub = $this->files->get(__DIR__.'/../../../installable/Crud_stubs/index.stub');

        $stub = str_replace('{{name_plural}}', $this->name_plural, $stub);
        $stub = str_replace('{{name_plural_lower}}', $this->name_plural_lower, $stub);

        return $stub;
    }

    protected function makePolicy(): void
    {
        $policy_path = base_path('/app/Policies/'.$this->name.'Policy.php');
        $this->info('Creating policy for '.$this->name.' - '.$policy_path);

        if ($this->files->exists($policy_path) && ! $this->option('force')) {
            $this->error('Policy file ('.$policy_path.') already exists. Skipping.');
            $this->line('');

            return;
        }

        $this->call('make:policy', [
            'name' => $this->name.'Policy',
            '--model' => $this->name,
        ]);

        $file = $this->files->get($policy_path);
        $file = str_replace('//', 'return true;', $file);
        $this->files->put($policy_path, $file);
        $this->line('');
    }

    protected function makeController(): void
    {
        $controller_path = $this->getAdminControllerPath();

        $this->info('Creating controller - '.$controller_path);

        if ($this->files->exists($controller_path) && ! $this->option('force')) {
            $this->error('Controller file ('.$controller_path.') already exists. Skipping.');
            $this->line('');

            return;
        }

        $this->makeDirectory(dirname($controller_path));

        $this->files->put($controller_path, $this->compileController());

        $this->info('Controller created.');
        $this->line('');
    }

    /**
     * Get the intended path to the config file
     */
    protected function getAdminControllerPath(): string
    {
        return base_path('/app/Http/Controllers/Admin/'.$this->name.'Controller.php');
    }

    protected function compileController(): string
    {
        $stub = $this->files->get(__DIR__.'/../../../installable/Crud_stubs/Controller.stub');

        $stub = str_replace('{{name_singular}}', $this->name, $stub);
        $stub = str_replace('{{name_plural}}', $this->name_plural, $stub);

        return $stub;
    }

    protected function makeModel(): void
    {
        $model_path = $this->getModelPath();

        $this->info('Making model - '.$model_path);

        $model_name = $this->name;

        if ($this->files->exists($model_path) && ! $this->option('force')) {
            $this->error('Model file ('.$model_path.') already exists. Skipping.');
            $this->line('');

            return;
        }

        $this->call('make:model', [
            '-m' => false,
            'name' => $model_name,
            '--force' => $this->option('force'),
        ]);

        $this->info('Model created.');
        $this->line('');
    }

    protected function makeMigration(): void
    {
        $this->call('make:migartion', [
            'name' => 'create_'.$this->name_plural_lower.'_table',
            '--force' => $this->option('force'),
        ]);

        $this->info('Migration created.');
        $this->line('');
    }

    protected function makeModelAndMigration(): void
    {
        $model_path = $this->getModelPath();

        $this->info('Making model and migration - '.$model_path);

        $model_name = $this->name;

        if ($this->files->exists($model_path) && ! $this->option('force')) {
            $this->error('Model file ('.$model_path.') already exists. Skipping.');
            $this->line('');

            return;
        }

        $this->call('make:model', [
            '-m' => true,
            'name' => $model_name,
            '--force' => $this->option('force'),
        ]);

        $this->info('Model and migration created.');
        $this->line('');
    }

    protected function getModelPath(): string
    {
        return base_path('/app/Models/'.$this->name.'.php');
    }

    /**
     * Create the config file that registers the CRUD
     */
    protected function makeConfigEntry(): void
    {
        $path = $this->getConfigPath();

        $this->info('Creating config entry - '.$path);

        if ($this->files->exists($path) && ! $this->option('force')) {
            $this->error('Config file ('.$path.') already exists. Skipping.');
            $this->line('');

            return;
        }

        $this->makeDirectory(dirname($path));

        $this->files->put($path, $this->compileConfigEntry());

        $this->info('Config created.');
        $this->line('');
    }

    /**
     * Get the path in which to store the config entry to register the CRUD
     */
    protected function getConfigPath(): string
    {
        return config_path('firefly/'.Str::snake($this->name).'.php');
    }

    /**
     * Create an the contents for the Laravel config file
     * that registers the CRUD
     */
    protected function compileConfigEntry(): string
    {
        $stub = $this->files->get(__DIR__.'/../../../installable/Crud_stubs/config.php');

        $stub = str_replace('{{name_singular}}', $this->name, $stub);
        $stub = str_replace('{{name_plural}}', $this->name_plural, $stub);
        $stub = str_replace('{{name_plural_lower}}', strtolower($this->name_plural), $stub);

        return $stub;
    }

    protected function makeFormRequest(): void
    {
        $request_path = $this->getRequestPath();

        $this->info('Creating form request - '.$request_path);

        if ($this->files->exists($request_path) && ! $this->option('force')) {
            $this->error('Form request file ('.$request_path.') already exists. Skipping.');
            $this->line('');

            return;
        }

        $this->makeDirectory(dirname($request_path));

        $this->files->put($request_path, $this->compileFormRequest());

        $this->info('Form Request created.');
        $this->line('');
    }

    protected function getRequestPath(): string
    {
        return app_path('Http/Requests/'.'Firefly'.$this->name.'Request.php');
    }

    protected function compileFormRequest(): string
    {
        $stub = $this->files->get(__DIR__.'/../../../installable/Crud_stubs/formRequest.stub');

        $stub = str_replace('{{name_singular}}', $this->name, $stub);

        return $stub;
    }
}
