<?php

namespace anima\firefly\Console\Commands;

use anima\firefly\Jobs\PublishPages as JobsPublishPages;
use App\Models\Page;
use Illuminate\Console\Command;

class PublishPages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pages:publish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish pages when their scheduled publish time is reached.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $pages = Page::readyToPublish()->get();

        if (count($pages) > 0) {
            JobsPublishPages::dispatch($pages);
        }
    }
}
