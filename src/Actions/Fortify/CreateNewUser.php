<?php

namespace App\Actions\Fortify;

use anima\firefly\Facades\Passkey;
use anima\firefly\Http\models\Tenant;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Contracts\CreatesNewUsers;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array<string, string>  $input
     */
    public function create(array $input): User
    {
        Validator::make($input, [
            'fname' => ['required', 'string', 'max:255'],
            'lname' => ['required', 'string', 'max:255'],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique(User::class),
            ],
            'gender' => ['required', 'string'],
            'dob' => ['required', 'date', 'before:18 years ago'],
            'country' => ['required', 'string'],
            'language' => ['required', 'string'],
            'account_type' => ['required', 'string'],
            'business_name' => ['string'],
            'business_role' => ['string'],
            'password' => $this->passwordRules(),
            'marketing' => ['string'],
            'privacy' => ['required'],
        ], [
            'fname.required' => 'The first name field is required',
            'lname.required' => 'The last name field is required',
            'dob.required' => 'The date of birth field is required',
            'dob.before' => 'The minimum user age for an Anima Hub account is 18.',
            'privacy.required' => 'You must agree to the privacy policy',
        ])->validate();

        return User::create([
            'fname' => $input['fname'],
            'lname' => $input['lname'],
            'email' => $input['email'],
            'gender' => $input['gender'],
            'dob' => $input['dob'],
            'country' => $input['country'],
            'language' => $input['language'],
            'account_type' => $input['account_type'],
            'business_name' => $input['account_type'] === 'Business' ? $input['business_name'] : null,
            'business_role' => $input['account_type'] === 'Business' ? $input['business_role'] : null,
            'password' => Hash::make($input['password']),
            'marketing' => array_key_exists('marketing', $input) ? 'all' : 'none',
            'tenant_id' => Tenant::current()->id,
            'user_passkey' => Passkey::generatePasskey(),
            'unsubcribe' => Str::random(100),
            // @TODO: add current version of privacy policy
            'privacy_policy_version' => '1.0.0',
        ]);
    }
}
