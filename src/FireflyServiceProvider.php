<?php

namespace anima\firefly;

use anima\firefly\Console\Commands\MakeContentType;
use anima\firefly\Console\Commands\MakeCrud;
use anima\firefly\Console\Commands\PublishPages;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class FireflyServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->publishes([
            __DIR__.'/../config/database.php' => config_path('database.php'),
            __DIR__.'/../config/firefly' => config_path('firefly/'),
        ], 'config');

        $this->publishes([
            __DIR__.'/../config/multitenancy.php' => config_path('multitenancy.php'),
        ], 'multitenancy-config');

    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        $this->mergeConfigFrom(__DIR__.'/../config/database.php', 'database');
        $this->mergeConfigFrom(__DIR__.'/../config/app.php', 'app');

        $installable_dir = __DIR__.'/../installable/';
        $resources_dir = __DIR__.'/../resources/';
        $database_dir = __DIR__.'/../database/';
        $src_dir = __DIR__.'/../src/';

        // Publish first time set up assets
        $this->publishes([
            // config files
            // $installable_dir.'tailwind.config.js' => base_path('tailwind.config.js'),
            // $installable_dir.'postcss.config.js' => base_path('postcss.config.js'),
            $installable_dir.'vite.config.js' => base_path('vite.config.js'),
            $installable_dir.'package.json' => base_path('package.json'),
            $installable_dir.'jsconfig.json' => base_path('jsconfig.json'),
            // $installable_dir . 'docker-compose.yml' => base_path('docker-compose.yml'),
            // $installable_dir . 'Docker' => base_path('Docker'),

            // web socket server files
            // $installable_dir.'socket/' => base_path('socket/'),

            // the service provider
            $installable_dir.'stubs/FireflyServiceProvider.php' => app_path('Providers/FireflyServiceProvider.php'),

            // css file
            $resources_dir.'css/app.css' => resource_path('css/app.css'),

            // frontend js files
            $resources_dir.'js/app.js' => resource_path('js/app.js'),
            $resources_dir.'js/vendor/firefly/Content' => resource_path('js/vendor/firefly/Content'),
            $resources_dir.'js/vendor/firefly/admin/ContentTypes' => resource_path('js/vendor/firefly/admin/ContentTypes'),

            // frontend auth pages
            $resources_dir.'js/vendor/firefly/Auth' => resource_path('js/Pages/Auth'),

            // frontend pages
            $resources_dir.'js/vendor/firefly/Pages' => resource_path('js/Pages'),

            // stubs for the Page model and controller
            $installable_dir.'stubs/PageModel.stub' => app_path('Models/Page.php'),
            $installable_dir.'stubs/PageController.stub' => app_path('Http/Controllers/PageController.php'),

            // stubs for the account controller
            $installable_dir.'stubs/AccountController.stub' => app_path('Http/Controllers/AccountController.php'),

            // database seeders
            $database_dir.'seeders' => database_path('seeders'),
        ], 'firefly-first-time-assets');

        // publish multiple publish groups
        $this->publishes([
            $resources_dir.'js/vendor/firefly/Auth' => resource_path('js/Pages/Auth'),
        ], 'firefly-auth');

        // Publish the db seeders
        $this->publishes([
            // $database_dir . 'data' => database_path('data'),
            $database_dir.'seeders' => database_path('seeders'),
        ], 'firefly-seeder');

        // Register routes
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');

        // Load migrations
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');

        // Load views
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'firefly');

        // Guess policy names
        Gate::guessPolicyNamesUsing(function (string $modelClass) {
            return 'anima\\firefly\\Http\\Policies\\'.class_basename($modelClass).'Policy';
        });

        // Register console commands
        if ($this->app->runningInConsole()) {
            $this->commands([
                MakeCrud::class,
                MakeContentType::class,
                PublishPages::class,
            ]);
        }
    }
}
