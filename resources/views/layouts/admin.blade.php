<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Firefly</title>
        @vite('resources/css/app.css')
    </head>
    <body>
        <div id="app" class="flex items-stretch w-screen">
            poo
            <side-panel 
                :current='{{json_encode($current)}}' 
                :menu='{{json_encode($items)}}'>
                <template #logout>
                    {{-- <form action="{{route('logout')}}" method="POST" class="flex">
                        @csrf
                        <button submit class="w-full flex items-center m-2 px-3 py-2 rounded-lg text-base font-bold hover:bg-admin-secondary">
                            <span class="text-lg w-4 mr-2">
                                <font-awesome-icon :icon="['fas', 'right-from-bracket']" />
                            </span>
                            Logout
                        </button>
                    </form> --}}
                </template>
            </side-panel>

            <div class="w-full p-6  ">
                @yield('main')
            </div>
        </div>
        {{-- @vite('resources/js/admin.js') --}}
        {{-- @vite('resources/js/app.js') --}}
    </body>
</html>