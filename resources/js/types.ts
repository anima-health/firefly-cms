export interface SearchResult {
    id: string;
    name: string;
    type: string;
    link: string;
    additional: any;
}

export interface UserId {
    id: string;
}