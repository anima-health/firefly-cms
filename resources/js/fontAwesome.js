/* import the fontawesome core */
import { library } from '@fortawesome/fontawesome-svg-core'

/* import font awesome icon component */
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

/* import specific icons */
import { faChevronLeft, faChevronRight, faXmark, faAddressCard, faHouse, faRightFromBracket } from '@fortawesome/free-solid-svg-icons'

/* add icons to the library */
library.add(faXmark, faChevronRight, faChevronLeft, faAddressCard, faHouse, faRightFromBracket)

export {FontAwesomeIcon}