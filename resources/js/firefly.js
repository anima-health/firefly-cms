import { createApp, h } from 'vue'
import { createInertiaApp } from '@inertiajs/vue3'
import { ZiggyVue } from '@vendor/tightenco/ziggy';


import Admin from './Pages/Layouts/Admin.vue';
/* import the fontawesome core */
import { library } from '@fortawesome/fontawesome-svg-core';

/* import font awesome icon component */
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

/* import specific icons */
import { faAddressCard, faArrowRightFromBracket, faBell, faBold, faCheck, faCircleExclamation, faEllipsisV, faFile, faGear, faGripVertical, faHotel, faHouse, faLayerGroup, faListUl, faPenToSquare, faPlusCircle, faSearch, faTrash, faTriangleExclamation, faUser, faUserCircle, faXmark } from '@fortawesome/free-solid-svg-icons';

library.add(
  faBold,
  faAddressCard,
  faHouse,
  faFile,
  faBell,
  faGear,
  faUser,
  faPlusCircle,
  faCheck,
  faXmark,
  faPenToSquare,
  faTrash,
  faCircleExclamation,
  faTriangleExclamation,
  faGripVertical,
  faEllipsisV,
  faArrowRightFromBracket,
  faUserCircle,
  faLayerGroup,
  faHotel,
  faListUl,
  faSearch,
)

createInertiaApp({
  resolve: name => {
    const pages = Object.assign(
      {}, 
      import.meta.glob('@pages/Pages/Admin/**/*.vue', { eager: true }), 
      import.meta.glob('@firefit/Pages/Admin/**/*.vue', { eager: true }), 
      import.meta.glob('./Pages/Admin/**/*.vue', { eager: true }),
      import.meta.glob('./Pages/Templates/Default.vue', { eager: true })
    );

    let page
    if (name.startsWith('FireflyFitness::')) {
      page = pages[`/vendor/anima/fireflyfitness/resources/js/Pages/${name.split('FireflyFitness::')[1]}.vue`]
    } else if (name.startsWith('Firefly::')) {
      page = pages[`./Pages/${name.split('Firefly::')[1]}.vue`]
    } else {
      page = pages[`/resources/js/Pages/${name}.vue`]
    }

    return page
  },
  setup({ el, App, props, plugin }) {
    createApp({ render: () => h(App, props) })
      .use(plugin)
      .use(ZiggyVue)
      .component('font-awesome-icon', FontAwesomeIcon)
      .mount(el)
  },
})