<?php

use anima\firefly\Http\controllers\admin\DashboardController;
use anima\firefly\Http\controllers\admin\MenuController;
use anima\firefly\Http\controllers\admin\PageController;
use anima\firefly\Http\controllers\admin\RedisController;
use anima\firefly\Http\controllers\admin\RoleController;
use anima\firefly\Http\controllers\admin\SettingController;
use anima\firefly\Http\controllers\admin\TenantController;
use anima\firefly\Http\controllers\admin\TiersController;
use anima\firefly\Http\controllers\admin\UserNotificationsController;
use anima\firefly\Http\controllers\admin\UsersController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['web', 'admin', 'tenant']], function () {
    Route::prefix('admin')->as('admin.')->group(function () {
        Route::get('/')->uses([DashboardController::class, 'index']);

        Route::resource('roles', RoleController::class);

        Route::resource('pages', PageController::class);
        Route::post('/pages/{id}/publish', [PageController::class, 'publish'])->name('admin.pages.publish');

        Route::resource('/tiers', TiersController::class);

        Route::resource('/users', UsersController::class);

        Route::resource('/menus', MenuController::class);

        Route::resource('/tenants', TenantController::class);

        Route::get('/settings')->uses([SettingController::class, 'index']);
        Route::post('/settings')->uses([SettingController::class, 'store']);

        Route::post('/store-redis', [RedisController::class, 'storeValue']);

        Route::post('/notifications/mark-all-read', [UserNotificationsController::class, 'markAllReadAdmin'])->name('admin.notifications.mark-all-read');
        Route::post('/notifications/delete/{id}', [UserNotificationsController::class, 'deleteAdmin'])->name('admin.notifications.delete');
        Route::post('/notifications/delete-all', [UserNotificationsController::class, 'deleteAllAdmin'])->name('admin.notifications.delete-all');
    });
});
