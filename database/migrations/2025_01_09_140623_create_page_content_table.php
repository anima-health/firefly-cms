<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::connection('content')->create('page_contents', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->unsignedBigInteger('page_id');
            $table->integer('order')->unsigned()->default(0)->index();
            $table->string('type');
            $table->json('content');
            $table->timestamps();

            $table->foreign('page_id')->references('id')->on('pages');
        });

        Schema::connection('content')->table('pages', function (Blueprint $table) {
            $table->dropColumn(['content', 'order', 'thumbnail_id']);
            $table->timestamp('published_at')->nullable()->after('published');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::connection('content')->dropIfExists('page_contents');

        Schema::connection('content')->table('pages', function (Blueprint $table) {
            $table->json('content')->nullable();
            $table->integer('order')->unsigned()->default(0)->index();
            $table->integer('thumbnail_id')->unsigned()->nullable()->index();
            $table->dropColumn('published_at');
        });
    }
};
