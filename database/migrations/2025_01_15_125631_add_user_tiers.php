<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tiers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();

            $table->unique(['name']);
        });

        Schema::create('model_has_tiers', function (Blueprint $table) {
            $table->unsignedBigInteger('tier_id');

            $table->string('model_type');
            $table->unsignedBigInteger('model_id');
            $table->index(['model_id', 'model_type'], 'model_has_tiers_model_id_model_type_index');

            $table->foreign('tier_id')
                ->references('id') // tier id
                ->on('tiers')
                ->onDelete('cascade');

            $table->primary(['tier_id', 'model_id', 'model_type'],
                'model_has_tiers_tier_model_type_primary');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::drop('model_has_tiers');
        Schema::drop('tiers');
    }
};
