<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('tenants', function (Blueprint $table) {
            $table->string('logo_url')->nullable()->after('database');
            $table->string('primary_color')->nullable()->after('logo_url');
            $table->string('secondary_color')->nullable()->after('primary_color');
        });

        Schema::table('features', function (Blueprint $table) {
            $table->bigInteger('tenant_id')->unsigned()->nullable()->after('id');
        });

        Schema::connection('content')->table('menu_items', function (Blueprint $table) {
            $table->bigInteger('tenant_id')->unsigned()->nullable()->after('id');
        });

        Schema::connection('content')->table('menus', function (Blueprint $table) {
            $table->bigInteger('tenant_id')->unsigned()->nullable()->after('id');
        });

        Schema::connection('content')->table('pages', function (Blueprint $table) {
            $table->bigInteger('tenant_id')->unsigned()->nullable()->after('id');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->bigInteger('tenant_id')->unsigned()->nullable()->after('id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('tenants', function (Blueprint $table) {
            $table->dropColumn(['logo_url', 'primary_color', 'secondary_color']);
        });

        Schema::table('features', function (Blueprint $table) {
            $table->dropColumn(['tenant_id']);
        });

        Schema::connection('content')->table('menu_items', function (Blueprint $table) {
            $table->dropColumn(['tenant_id']);
        });

        Schema::connection('content')->table('menus', function (Blueprint $table) {
            $table->dropColumn(['tenant_id']);
        });

        Schema::connection('content')->table('pages', function (Blueprint $table) {
            $table->dropColumn(['tenant_id']);
        });

        Schema::connection('content')->table('users', function (Blueprint $table) {
            $table->dropColumn(['tenant_id']);
        });
    }
};
