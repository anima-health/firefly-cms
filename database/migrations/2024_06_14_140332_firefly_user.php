<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // remoe the name column
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('name');
        });

        // add in the columns used for firefly
        Schema::table('users', function (Blueprint $table) {
            $table->string('fname')->after('id');
            $table->string('lname')->after('fname');
            $table->unsignedBigInteger('gender')->nullable()->after('password');
            // $table->unsignedBigInteger('tenant_id')->nullable();
            $table->dateTime('dob')->nullable()->after('gender');
            $table->string('country')->nullable()->after('dob');
            $table->string('language')->nullable()->after('country');
            $table->string('account_type')->after('language');
            $table->string('business_role')->nullable()->after('account_type');
            $table->string('business_name')->nullable()->after('business_role');
            $table->double('weight')->nullable()->after('business_name');
            $table->string('profile_image')->nullable()->after('weight');
            $table->string('marketing')->nullable()->after('profile_image');
            $table->string('unsubcribe')->after('marketing');

            // define foreign key for tenant
            // $table->foreign('tenant_id')->references('id')->on('tenants')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn([
                'fname',
                'lname',
                'gender',
                // 'tenant_id',
                'dob',
                'country',
                'language',
                'account_type',
                'business_role',
                'business_name',
                'weight',
                'profile_image',
                'marketing',
            ]);
        });

        Schema::table('users', function (Blueprint $table) {
            $table->string('name');
        });
    }
};
