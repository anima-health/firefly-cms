const http = require('http');
const { Server } = require('socket.io');
const redisAdapter = require('socket.io-redis');
const redis = require('redis');

// Create HTTP server
const server = http.createServer();
require('dotenv').config();

const io = new Server(server, {
    cors: {
        origin: process.env.APP_URL, // Update to match your front-end URL from .env file
        methods: ["GET", "POST"],
        credentials: true,
    },
});

// Use Redis as the adapter for Socket.IO
// io.adapter(redisAdapter({ host: 'redis', port: 6379 }));

// Connect to Redis
const redisClient = redis.createClient({ host: 'redis', port: 6379 });

redisClient.on('connect', () => {
    console.log('Connected to Redis');
});

redisClient.on('error', (err) => {
    console.error('Redis connection error:', err);
});


// Subscribe to the Redis channel
redisClient.subscribe('notifications', (err) => {
    if (err) {
        console.error('Failed to subscribe to notifications channel:', err);
    } else {
        console.log('Successfully subscribed to notifications channel');
    }
});

// Listen for events from Redis
redisClient.on('message', (channel, message) => {
    console.log('Received channel notification:', message);
    if (channel === 'notifications') {
        // Emit to all connected clients via Socket.IO
        io.emit('notification', JSON.parse(message));
    }
});

io.on('connection', (socket) => {
    console.log('A user connected:', socket.id);
    socket.on('disconnect', () => {
        console.log('User disconnected');
    });
});

// Start Socket.IO server
server.listen(3000, () => {
    console.log('Socket.IO server running on port 3000');
});
