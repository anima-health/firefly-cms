// vite.config.js
import vue from "@vitejs/plugin-vue";
import laravel from "laravel-vite-plugin";
import path from "path";
import { defineConfig } from "vite";
import tailwindcss from "@tailwindcss/vite";

export default defineConfig({
    plugins: [
        vue(),
        tailwindcss(),
        laravel({
            input: [
                'resources/js/app.js', // Admin entry point
                'resources/js/admin.js', // Public entry point
                'resources/css/app.css'
            ],
            refresh: true,
        }),
    ],
    build: {
        rollupOptions: {
            output: {
                entryFileNames: 'assets/[name]-[hash].js',
                chunkFileNames: 'assets/[name]-[hash].js',
                assetFileNames: 'assets/[name]-[hash].[ext]',
            },
        },
    },
    resolve: {
        alias: {
            vue: "vue/dist/vue.esm-bundler",
            "@pages": path.resolve(
                __dirname,
                "./resources/js"
            ),
            "@": path.resolve(
                __dirname,
                "./vendor/anima/firefly/resources/js/vendor/firefly"
            ),
            "@fire": path.resolve(
                __dirname,
                "./vendor/anima/firefly/resources/js"
            ),
            "@fireIMG": path.resolve(
                __dirname,
                "./vendor/anima/firefly/resources/img"
            ),
            "@vendor":  path.resolve(
                __dirname,
                "./vendor/"
            ),
            '@firefit':  path.resolve(
                __dirname,
                "./vendor/anima/fireflyfitness/resources/js"
            ),
        },
    },
});
