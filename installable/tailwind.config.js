/** @type {import('tailwindcss').Config} */
export default {
    content: [
        "./resources/**/*.blade.php",
        "./resources/**/*.js",
        "./resources/**/*.vue",
        "./vendor/anima/firefly/resources/js/vendor/firefly/**/*.vue",
        "./vendor/anima/firefly/resources/js/**/*.vue",
    ],
    theme: {
        extend: {
            keyframes: {
                slide: {
                    '0%': {
                        transform: 'translateY(110%)',
                    },
                    '100%': {
                        transform: 'translateY(0%)',
                    },
                },
                center_left: {
                    '0%': {
                        transform: 'translateX(0%)',
                    },
                    '100%': {
                        transform: 'translateX(-100%)',
                    },
                },
                left_center: {
                    '0%': {
                        transform: 'translateX(-100%)',
                    },
                    '100%': {
                        transform: 'translateX(0%)',
                    },
                },
                right_center: {
                    '0%': {
                        transform: 'translateX(100%)',
                    },
                    '100%': {
                        transform: 'translateX(0%)',
                    },
                },
                center_right: {
                    '0%': {
                        transform: 'translateX(0%)',
                    },
                    '100%': {
                        transform: 'translateX(300%)',
                    },
                },
                open_panel: {
                    '0%': {
                        height: '0',
                    },
                    '100%': {
                        height: '500px',
                    },
                },
                grow_margin: {
                    '0%': {
                        'margin-bottom': '0',
                    },
                    '100%': {
                        'margin-bottom': '14rem',
                    },
                },
            },

            animation: {
                'slide-up': 'slide .3s linear',
                'slide-down': 'slide .3s reverse forwards',
                'slide-next-in': 'right_center .2s linear forwards',
                'slide-next-out': 'center_left .2s linear forwards',
                'slide-prev-in': 'left_center .2s linear forwards',
                'slide-prev-out': 'center_right .2s linear forwards',
                'open-dash': '.3s open_panel .3s linear forwards',
                'grow-dash': '.2s grow_margin .3s linear forwards',
            },
            
            colors: {
                "firefly-yellow": '#FFCB2E',
                "primary": 'var(--blue)',
                "firefly-orange": '#FF7400',
                "firefly-light": '#FFEFBE',
                "firefly-green": '#00A86B',
                "firefly-teal": '#00A6A6',
                "firefly-midnight-blue": '#002D72',
            },
        },
    },
    plugins: [],
}
