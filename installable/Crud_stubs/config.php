<?php

return [
    'name' => '{{name_plural}}',
    'model' => 'App\Models\{{name_singular}}',
    'icon' => 'fa-address-card ',
    'index' => '/admin/{{name_plural_lower}}',
    'gate' => 'viewAny',
    'section' => [
        'id' => 3,
        'name' => 'Site',
    ],
];
