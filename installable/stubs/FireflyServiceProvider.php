<?php

namespace App\Providers;

use anima\firefly\Facades\FireflyMenu;
use anima\firefly\Http\models\User;
use anima\firefly\Services\FireflyMenuService;
use anima\firefly\Services\PasskeyService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\ServiceProvider;
use Inertia\Inertia;
use Laravel\Fortify\Fortify;

class FireflyServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        $router = $this->app['router'];
        $router->pushMiddlewareToGroup('admin', \anima\firefly\Http\Middleware\CmsAccess::class);

        Fortify::authenticateUsing(function (Request $request) {
            $user = User::where('email', $request->email)->first();

            if ($user && Hash::check($request->password, $user->password)) {
                return $user;
            }

            return null;
        });

        $this->app->bind('passkey', function () {
            return new PasskeyService;
        });

        $this->app->bind('fireflymenu', function () {
            return new FireflyMenuService;
        });

        Fortify::loginView(function () {
            return Inertia::render('Auth/Login', ['menu' => FireflyMenu::getMenu()]);
        });

        Fortify::registerView(function () {
            return Inertia::render('Auth/Register', ['menu' => FireflyMenu::getMenu()]);
        });

        // @FIXME: needs emails to be working properly first for these to work

        // Fortify::requestPasswordResetLinkView(function () use ($menuData) {
        //     return Inertia::render('Auth/ForgotPassword', ['menu' => $menuData]);
        // });

        // Fortify::resetPasswordView(function () use ($menuData) {
        //     return Inertia::render('Auth/ResetPassword', ['menu' => $menuData]);
        // });

        // Fortify::verifyEmailView(function () use ($menuData) {
        //     return Inertia::render('Auth/VerifyEmail', ['menu' => $menuData]);
        // });
    }
}
